The following are some projects I have done or worked on that have been published to App Store:


----
##Web:

* ASXData (Solo Work Python)
    [https://www.asxdata.com.au/](https://www.asxdata.com.au/)
    
* China Austrlia Film Festival (第三届中澳电影节)
    [https://caiff.com.au/index.php](https://caiff.com.au/index.php)

* CPSkin Care E-commerce (Solo Work PHP)
    [http://cpskin.com.au](http://cpskin.com.au)

* RedBroking Web (Solo Work PHP)
    [https://redbroking.com/](https://redbroking.com/)
    
* OzSwitch (Solo Work PHP)
    [http://www.ozswitch.com/](http://www.ozswitch.com/)

* Wheat PTI Data Management System (Solo Work Python)
    [https://wheatpti.backpaddock.com.au:8889/](https://wheatpti.backpaddock.com.au:8889/)
    
    

##Objective-C:
 
* Back Paddock Mobile (Team Work)
    [https://itunes.apple.com/au/app/back-paddock-mobile-hd/id608154225?mt=8](https://itunes.apple.com/au/app/back-paddock-mobile-hd/id608154225?mt=8)

* Wheat PTI (Solo Work)
    [https://itunes.apple.com/au/app/wheat-pti/id1159180918?mt=8](https://itunes.apple.com/au/app/wheat-pti/id1159180918?mt=8 )

* My Shopping List (Solo Work)
    [https://itunes.apple.com/au/app/my-shopping-list/id1086813489](https://itunes.apple.com/au/app/my-shopping-list/id1086813489)
 
##Swift:

* My NBN (Solo Work)
    [https://itunes.apple.com/us/app/my-nbn/id1247260936?mt=8](https://itunes.apple.com/us/app/my-nbn/id1247260936?mt=8)

* Sino-Aus Film Festival ((Solo Work)) 
    [https://itunes.apple.com/au/app/%E4%B8%AD%E6%BE%B3%E5%9B%BD%E9%99%85%E7%94%B5%E5%BD%B1%E8%8A%82/id1159227021?mt=8 ](https://itunes.apple.com/au/app/%E4%B8%AD%E6%BE%B3%E5%9B%BD%E9%99%85%E7%94%B5%E5%BD%B1%E8%8A%82/id1159227021?mt=8 )

    


----
##Open Source Project:
* React Component (Solo Work JavaScript React Node, work on both web and mobile)
    [https://www.npmjs.com/package/react-appstore-button](https://www.npmjs.com/package/react-appstore-button)
* Python Teletra SMS SDK (Solo work Python)
    [https://pypi.python.org/pypi/telstra-sms-sdk](https://pypi.python.org/pypi/telstra-sms-sdk)

----

## More about myself

Fore more information, you can find me:

* [Web](http://arkilis.me)
* [Medium](https://medium.com/@arkilis)
* [Twitter](https://twitter.com/arkilis) 


Thanks for reading!




